#Test 1
from typing import List

from sqlalchemy import desc
from sqlalchemy.orm import Session

from models import Recipe


def get_recipes_sorted(db: Session, skip: int = 0, limit: int = 10) -> List[Recipe]:
    """
    Получение отсортированного списка рецептов.

    Args:
        db (Session): Сессия базы данных.
        skip (int): Количество рецептов для пропуска.
        limit (int): Лимит количества возвращаемых рецептов.

    Returns:
        List[Recipe]: Отсортированный список рецептов.
    """
    return (
        db.query(Recipe)
        .order_by(desc(Recipe.views), Recipe.cooking_time)
        .offset(skip)
        .limit(limit)
        .all()
    )


def get_recipe_by_id(db: Session, recipe_id: int) -> Recipe:
    """
    Получение рецепта по его уникальному идентификатору.

    Args:
        db (Session): Сессия базы данных.
        recipe_id (int): Уникальный идентификатор рецепта.

    Returns:
        Recipe: Информация о рецепте.
    """
    return db.query(Recipe).filter(Recipe.id == recipe_id).first()
