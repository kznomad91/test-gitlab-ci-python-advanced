#Test 1
from typing import List

import uvicorn
from fastapi import Depends, FastAPI, HTTPException
from sqlalchemy.orm import Session

import crud
import models
import schemas
from database import SessionLocal, engine
from seed import seed_db

app = FastAPI()
models.Base.metadata.create_all(bind=engine)


def get_db() -> Session:
    """
    Функция для управления сессиями базы данных.

    Returns:
        Session: Сессия базы данных.
    """
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


@app.get("/recipes/", response_model=List[schemas.Recipe])
def get_recipes(
    skip: int = 0, limit: int = 10, db: Session = Depends(get_db)
) -> List[models.Recipe]:
    """
    Обработчик для получения списка рецептов.

    Args:
        skip (int): Количество рецептов для пропуска.
        limit (int): Лимит количества возвращаемых рецептов.
        db (Session): Сессия базы данных.

    Returns:
        List[models.Recipe]: Список рецептов.
    """
    recipes = crud.get_recipes_sorted(db, skip, limit)
    return recipes


@app.get("/recipes/{recipe_id}", response_model=schemas.Recipe)
def get_recipe(recipe_id: int, db: Session = Depends(get_db)) -> models.Recipe:
    """
    Обработчик для получения информации о конкретном рецепте.

    Args:
        recipe_id (int): Уникальный идентификатор рецепта.
        db (Session): Сессия базы данных.

    Returns:
        models.Recipe: Информация о рецепте.
    """
    recipe = crud.get_recipe_by_id(db, recipe_id)
    if recipe is None:
        raise HTTPException(status_code=404, detail="Recipe not found")
    recipe.increase_views(db)
    return recipe


if __name__ == "__main__":
    db = SessionLocal()
    seed_db(db)
    db.close()

    uvicorn.run(app, host="127.0.0.1", port=8000)
