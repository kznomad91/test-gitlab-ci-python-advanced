#Test 1
from sqlalchemy.orm import Session

from models import Recipe


def seed_db(db: Session) -> None:
    """
    Заполнение базы данных исходными рецептами.

    Args:
        db (Session): Сессия базы данных.
    """
    recipes = [
        {
            "title": "Плов",
            "cooking_time": 60,
            "ingredients": "Рис, мясо, морковь, лук, масло",
            "description": "Традиционное блюдо восточной кухни",
        },
        {
            "title": "Омлет",
            "cooking_time": 15,
            "ingredients": "Яйца, молоко, соль, перец, овощи (по желанию)",
            "description": "Простое блюдо из яиц",
        },
    ]
    for recipe_data in recipes:
        db_recipe = Recipe(**recipe_data)
        db.add(db_recipe)
    db.commit()
    db.close()
