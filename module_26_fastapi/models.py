#Test 1
from sqlalchemy import Column, Integer, String, Text, update
from sqlalchemy.orm import Session

from database import Base


class Recipe(Base):
    """
    Модель рецепта.

    Attributes:
        id (int): Уникальный идентификатор рецепта.
        title (str): Название рецепта.
        cooking_time (int): Время приготовления рецепта в минутах.
        ingredients (str): Перечисление ингредиентов рецепта.
        description (str): Описание рецепта.
        views (int): Количество просмотров рецепта.

    Methods:
        increase_views(db: Session) -> None: Увеличивает количество просмотров рецепта.
    """

    __tablename__ = "recipes"
    id: int = Column(Integer, primary_key=True, index=True)
    title: str = Column(String, index=True, unique=True, nullable=False)
    cooking_time: int = Column(Integer, nullable=False)
    ingredients: str = Column(Text, nullable=False)
    description: str = Column(Text, nullable=False)
    views: int = Column(Integer, default=0)

    def increase_views(self, db: Session) -> None:
        """
        Увеличивает количество просмотров рецепта.

        Args:
            db (Session): Сессия базы данных.
        """
        self.views += 1
        db.execute(update(Recipe).where(Recipe.id == self.id).values(views=self.views))
