#Test 1
from typing import List

from pydantic import BaseModel


class RecipeBase(BaseModel):
    """
    Базовая схема для описания полей рецепта.
    """

    title: str
    cooking_time: int
    ingredients: str
    description: str


class Recipe(RecipeBase):
    """
    Схема для возврата данных рецепта через API.
    """

    id: int
    views: int

    class Config:
        orm_mode = True
